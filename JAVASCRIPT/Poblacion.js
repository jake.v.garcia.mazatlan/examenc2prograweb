document.addEventListener("DOMContentLoaded", function() {
    function generarNumeroAleatorio(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    function clasificarEdades(edades) {
        let bebe = 0;
        let niño = 0;
        let adolescente = 0;
        let adulto = 0;
        let anciano = 0;
        let sumaEdades = 0;

        for (let i = 0; i < edades.length; i++) {
            const edad = edades[i];
            sumaEdades += edad;

            if (edad >= 1 && edad <= 3) {
                bebe++;
            } else if (edad >= 4 && edad <= 12) {
                niño++;
            } else if (edad >= 13 && edad <= 17) {
                adolescente++;
            } else if (edad >= 18 && edad <= 60) {
                adulto++;
            } else {
                anciano++;
            }
        }

        const promedio = sumaEdades / edades.length;

        document.getElementById("bebe").value = bebe;
        document.getElementById("niño").value = niño;
        document.getElementById("adolescente").value = adolescente;
        document.getElementById("adulto").value = adulto;
        document.getElementById("anciano").value = anciano;
        document.getElementById("promedio").value = promedio.toFixed(2);
    }

    // Función para generar edades aleatorias
    function generarArreglo() {
        const edades = [];
        const cantidad = 100;

        for (let i = 0; i < cantidad; i++) {
            edades.push(generarNumeroAleatorio(0, 90));
        }

        document.getElementById("edad").value = edades.join(", ");
        clasificarEdades(edades);
    }

    

    // Función para limpiar los campos
    function limpiarCampos() {
        document.getElementById("edad").value = "";
        document.getElementById("bebe").value = "";
        document.getElementById("niño").value = "";
        document.getElementById("adolescente").value = "";
        document.getElementById("adulto").value = "";
        document.getElementById("anciano").value = "";
        document.getElementById("promedio").value = "";
    }

    document.getElementById("btnGenerar").addEventListener("click", function() {
        generarArreglo();
    });

    document.getElementById("btnLimpiar").addEventListener("click", function() {
        limpiarCampos();
    });
});
